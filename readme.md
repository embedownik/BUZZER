# BUZZER - module for buzzer control for STM32HAL


How to use:

- init STM32CubeIDE project
- init timer with PWM output
- create config struct to pass to Init function:

```C
static buzzer_config_s buzzerConfig =
{
     .timerHandler = &htim2,
     .timerFreq    = 100000U,
     .channel      = TIM_CHANNEL_3,
};

buzzer_Init(buzzerConfig);
```

- add file "board.h" to project
- in board.h include platform/HAL headers; for example:

```C
#include <stm32f3xx_hal.h>
#include <stm32f3xx.h>
```