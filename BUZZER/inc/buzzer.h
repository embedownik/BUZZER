/*
 * buzzer.h
 *
 *      Author: embedownik
 */

#ifndef BUZZER_BUZZER_INC_BUZZER_H_
#define BUZZER_BUZZER_INC_BUZZER_H_

#include <stdint.h>

#include <board.h>

/**
 * Struct with module parameters - used in Init function.
 */
typedef struct
{
    TIM_HandleTypeDef *timerHandler; /* timer to use */
    uint32_t timerFreq;              /* Given timer tick frequency */
    uint32_t channel;                /* channel to use */
}buzzer_config_s;

void buzzer_Init(buzzer_config_s config);

void buzzer_Play(uint16_t freq);

void buzzer_Stop(void);

#endif /* BUZZER_BUZZER_INC_BUZZER_H_ */
