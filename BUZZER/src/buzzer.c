#include <buzzer.h>

static buzzer_config_s actualConfig;

static void setFrequency(uint16_t freq)
{
    const uint32_t base = actualConfig.timerFreq;

    uint16_t ARR = base / freq;
    uint16_t PWM = ARR / 2;

    __HAL_TIM_SET_AUTORELOAD(actualConfig.timerHandler, ARR);
    __HAL_TIM_SET_COUNTER(actualConfig.timerHandler, 0U);
    __HAL_TIM_SET_COMPARE(actualConfig.timerHandler, actualConfig.channel, PWM);
}

static void stop(void)
{
    __HAL_TIM_SET_AUTORELOAD(actualConfig.timerHandler, 10U);
    __HAL_TIM_SET_COUNTER(actualConfig.timerHandler, 0U);
    __HAL_TIM_SET_COMPARE(actualConfig.timerHandler, actualConfig.channel, 11U);
}

void buzzer_Init(buzzer_config_s config)
{
    actualConfig = config;

    HAL_TIM_Base_Start(actualConfig.timerHandler);

    stop();

    HAL_TIM_PWM_Start(actualConfig.timerHandler, actualConfig.channel);

    __HAL_TIM_ENABLE(actualConfig.timerHandler);
}

void buzzer_Play(uint16_t freq)
{
    if(freq == 0U)
    {
        stop();
    }
    else
    {
        setFrequency(freq);
    }
}

void buzzer_Stop(void)
{
    stop();
}
